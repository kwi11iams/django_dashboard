from django.conf import settings
from django.db import models
import django_tables2 as tables

# Create your models here.

# TYPE_CHOICES = (
#     ('Poly: Known', 'Poly: Known'),
#     ('UV / Mut?', 'UV / Mut?'),
#     ('Poly: gnomAD', 'Poly: gnomAD'),
#     ('UV / Mut? - HGMD', 'Poly: gnomAD'),
#     ('Class 2', 'Class 2'),
#     ('UV / Mut? - KnownPathogenic', 'UV / Mut? - KnownPathogenic'),
# )


class Variant(models.Model):
    variant_ID = models.IntegerField(primary_key=True)
    g_coord = models.IntegerField(null=True, blank=True)
    gene = models.CharField(max_length=50, null=True, blank=True)
    ref_base = models.CharField(max_length=100, null=True, blank=True, verbose_name="ref")
    alt_base = models.CharField(max_length=100, null=True, blank=True, verbose_name="alt")
    ref_seq = models.CharField(max_length=100, null=True, blank=True)
    chr = models.CharField(max_length=2, null=True, blank=True)
    variant_type = models.CharField(max_length=100, null=True, blank=True)
    genome_change = models.CharField(max_length=200, null=True, blank=True)
    c_nomen = models.CharField(max_length=100, null=True, blank=True, verbose_name=" c.nomen")
    p_nomen = models.CharField(max_length=100, null=True, blank=True, verbose_name=" p.nomen")
    g_nomen = models.CharField(max_length=200, null=True, blank=True, verbose_name=" g.nomen")
    alamut = models.CharField(max_length=200, null=True, blank=True)
    HGMD_DNA = models.CharField(max_length=200, null=True, blank=True)
    build = models.CharField(max_length=20, null=True, blank=True)
    pred_artefact = models.BooleanField(null=True, blank=True)

    def __str__(self):
        return self.gene


class Occurence(models.Model):
    worklist = models.IntegerField(null=True, blank=True)
    starlims_ID = models.CharField(max_length=50, null=True, blank=True)
    mid = models.CharField(max_length=50, null=True, blank=True)
    panel = models.CharField(max_length=50, null=True, blank=True)
    coverage = models.CharField(max_length=200, null=True, blank=True)
    frequency = models.CharField(max_length=200, null=True, blank=True)
    score = models.CharField(max_length=200, null=True, blank=True)
    homo_hetero = models.CharField(max_length=200, null=True, blank=True)
    poss_artefact = models.BooleanField(null=True, blank=True)
    variant_type = models.CharField(max_length=100, null=True, blank=True)
    variant_ID = models.ForeignKey(Variant, on_delete=models.CASCADE)
    # def count_occurences(self):
    #     return self.variant_ID.count()


class Denominators(models.Model):
    panel = models.CharField(max_length=200, null=True, blank=True)
    gene = models.CharField(max_length=50, null=True, blank=True)
    total = models.IntegerField(null=True, blank=True)


class Variant_type(models.Model):
    def __str__(self):
        return self.type
    type = models.CharField(max_length=30, null=True, blank=True)

# class VariantTable(tables.Table):
#     class Meta:
#         model = Variant
