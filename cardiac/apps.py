from django.apps import AppConfig


class CardiacConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cardiac'
