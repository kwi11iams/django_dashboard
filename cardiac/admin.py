from django.contrib import admin
from .models import Occurence, Variant, Denominators, Variant_type

admin.site.register(Occurence)
admin.site.register(Variant)
admin.site.register(Denominators)
admin.site.register(Variant_type)
