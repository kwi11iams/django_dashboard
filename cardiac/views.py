from django.shortcuts import render, get_object_or_404
from django_tables2 import SingleTableView, SingleTableMixin
from django_tables2.export.views import ExportMixin
from django.http import HttpResponse
from .models import Variant, Occurence, Denominators
from .tables import VariantTable

from .forms import PS4Form
from .filters import VariantFilter
import django_filters
from django_filters.views import FilterView

from math import exp, log, sqrt
import scipy.stats
import csv


class VariantListView(ExportMixin, SingleTableMixin, FilterView):
    model = Variant
    table_class = VariantTable
    template_name = 'cardiac/table.html'
    filterset_class = VariantFilter
    exclude_columns = ("view_variant", )


def variant_details_csv(request, pk):
    variant = get_object_or_404(Variant, pk=pk)

    # Variant nomenclature
    variant.genome_change_edit = variant.genome_change.split(":")[1]
    if variant.p_nomen != "":
        protein_change = variant.p_nomen.split(".")[1]
        variant.p_nomen_edit = " p.(" + protein_change + ")"
    else:
        variant.p_nomen_edit = variant.p_nomen
    variant_name1 = variant.ref_seq + "(" + variant.gene + "):" + \
        variant.c_nomen + variant.p_nomen_edit
    variant_name2 = "Chr" + variant.chr + "(" + variant.build + "):" + variant.genome_change_edit

    variant_filename = variant.ref_seq + "(" + variant.gene + ")" + variant.c_nomen + ".csv"
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="{}"'.format(variant_filename)
    panels = get_panel_info(pk)
    denom = get_denom_info(pk)

    writer = csv.writer(response)
    writer.writerow([variant_name1])
    writer.writerow([variant_name2])
    writer.writerow([])
    writer.writerow(['Variant Details'])
    writer.writerow(['g.nomen', 'Gene', 'Ref', 'Alt', 'Ref Seq',
                    'Variant Type', 'c.nomen', 'p.nomen'])
    writer.writerow([variant.g_nomen, variant.gene, variant.ref_base, variant.alt_base,
                    variant.ref_seq, variant.variant_type, variant.c_nomen, variant.p_nomen])
    writer.writerow([])
    writer.writerow(['Summary Cases'])
    writer.writerow(['Condition', 'Cases', 'Homozygotes', 'Denominators'])

    try:
        writer.writerow(['DCM', panels["DCM"][0], panels["DCM"][1], denom["DCM"]])
    except:
        pass
        # writer.writerow(['DCM', panels["DCM"][0], panels["DCM"][1]])
    try:
        writer.writerow(['HCM', panels["HCM"][0], panels["HCM"][1], denom["HCM"]])
    except:
        pass
        # writer.writerow(['HCM', panels["HCM"][0], panels["HCM"][1]])
    try:
        writer.writerow(['ARVC', panels["ARVC"][0], panels["ARVC"][1], denom["ARVC"]])
    except:
        pass
        # writer.writerow(['ARVC', panels["ARVC"][0], panels["ARVC"][1]])
    writer.writerow(['Total', panels["Total"][0], panels["Total"][1]])
    return response


def denominators(request):
    panels = Denominators.objects.order_by().values_list('panel').distinct()
    z = []
    for p in panels:
        z.append([p, Denominators.objects.filter(panel=p[0]).values_list('gene', 'total')])
    return render(request, 'cardiac/denominators.html', {'denominators': z})


def get_panel_info(pk):
    occurences = Occurence.objects.filter(variant_ID=pk)
    DCM = occurences.filter(panel__contains='DCM')
    DCM_homo = len(DCM.filter(homo_hetero__contains='Homo'))
    HCM = occurences.filter(panel__contains='HCM')
    HCM_homo = len(HCM.filter(homo_hetero__contains='Homo'))
    ARVC = occurences.filter(panel__contains='ARVC')
    ARVC_homo = len(ARVC.filter(homo_hetero__contains='Homo'))
    panels = {'DCM': [len(DCM), DCM_homo], 'HCM': [len(HCM), HCM_homo], 'ARVC': [len(
        ARVC), ARVC_homo], 'Total': [len(DCM)+len(HCM)+len(ARVC), DCM_homo+HCM_homo+ARVC_homo]}
    return panels


def get_denom_info(pk):
    variant = get_object_or_404(Variant, pk=pk)
    gene1 = variant.gene
    denom_gene = Denominators.objects.filter(gene=gene1).values()
    denom = {}
    for i in denom_gene:
        if i["panel"] == "R132_Twist_DCM-ArCM":
            denom["DCM"] = i["total"]
        elif i["panel"] == "R131_Twist_HCM":
            denom["HCM"] = i["total"]
        elif i["panel"] == "R133_Twist_ARVC":
            denom["ARVC"] = i["total"]
    return denom


def variant_detail(request, pk):
    variant = get_object_or_404(Variant, pk=pk)
    occurences = Occurence.objects.filter(variant_ID=pk)

    panels = get_panel_info(pk)

    # gather denominator information for given gene of variant
    denom = get_denom_info(pk)

    # generate gnomad website link
    chr = variant.chr
    g_coord = str(variant.g_coord)
    ref = variant.ref_base
    alt = variant.alt_base
    gnomad_var = "-".join([chr, g_coord, ref, alt])
    gnomad_link = "https://gnomad.broadinstitute.org/variant/"+gnomad_var+"?dataset=gnomad_r2_1"

    # generate alamut link
    alamut_change = variant.alamut.split(":")[1]
    alamut_link = chr + "(GRCh37):g." + alamut_change
    # c_nomen_edit = variant.c_nomen.split("[")[1].split("]")[0]
    # alamut_link = variant.gene + ":c." + c_nomen_edit
    # alamut_link = variant.ref_seq + ":c." + c_nomen_edit

    # generate strings for nomenclature
    variant.genome_change_edit = variant.genome_change.split(":")[1]
    if variant.p_nomen != "":
        protein_change = variant.p_nomen.split(".")[1]
        variant.p_nomen_edit = " p.(" + protein_change + ")"
    else:
        variant.p_nomen_edit = variant.p_nomen
    return render(request, 'cardiac/variant_detail.html', {'variant': variant, 'occurences': occurences, 'panels': panels, "denominators": denom, "gnomad_link": gnomad_link, "alamut_link": alamut_link})


def odds_ratio(cases_AC, controls_AC, cases_AN, controls_AN):
    a = cases_AC
    b = cases_AN - cases_AC
    c = controls_AC
    d = controls_AN - controls_AC
    OR = (a*d)/(b*c)
    return OR, a, b, c, d


def ps4_calculator(cases_AC, controls_AC, cases_AN, controls_AN, CI):
    OR, a, b, c, d = odds_ratio(cases_AC, controls_AC, cases_AN, controls_AN)
    z = scipy.stats.norm.ppf(1-((100-CI)*0.01)/2)
    OR_lower = exp(log(OR) - z*sqrt(1/a + 1/b + 1/c + 1/d))
    return OR_lower


def ps4_result(request):
    if request.method == "POST":
        form = PS4Form(request.POST)
        if form.is_valid():
            cases = form.cleaned_data['cases_AC']
            cases_total = form.cleaned_data['cases_AN']
            controls = form.cleaned_data['controls_AC']
            controls_total = form.cleaned_data['controls_AN']

            OR = odds_ratio(cases, controls, cases_total, controls_total)[0]

            strong_OR = form.cleaned_data['strong_OR']
            strong_CI = form.cleaned_data['strong_CI']
            moderate_OR = form.cleaned_data['moderate_OR']
            moderate_CI = form.cleaned_data['moderate_CI']
            supporting_OR = form.cleaned_data['supporting_OR']
            supporting_CI = form.cleaned_data['supporting_CI']

            strong_lower = ps4_calculator(cases, controls, cases_total, controls_total, strong_CI)
            m_lower = ps4_calculator(cases, controls, cases_total, controls_total, moderate_CI)
            sup_lower = ps4_calculator(cases, controls, cases_total, controls_total, supporting_CI)

            text1 = "{} in {} case alleles vs {} in {} control alleles gives an odds ratio of {}".format(
                cases, cases_total, controls, controls_total, round(OR, 5))
            text2 = "Lower bound odds ratios:"
            text3 = "Strong {}% CI={} (threshold for strong ≥{})".format(
                strong_CI, round(strong_lower, 2), strong_OR)
            text4 = "Moderate {}% CI={} (threshold for moderate ≥{})".format(
                moderate_CI, round(m_lower, 2), moderate_OR)
            text5 = "Supporting {}% CI={} (threshold for supporting ≥{})".format(
                supporting_CI, round(sup_lower, 2), supporting_OR)

            if strong_lower >= strong_OR:
                text6 = "The lower bound {}% CI is greater than {} ({}%CI={}). Therefore PS4 is set to".format(
                    strong_CI, strong_OR, strong_CI, round(strong_lower, 2))
                text7 = " PS4_Strong"
            elif m_lower >= moderate_OR:
                text6 = "The lower bound {}% CI is greater than {} ({}%CI={}). Therefore PS4 is set to".format(
                    moderate_CI, moderate_OR, moderate_CI, round(m_lower, 2))
                text7 = " PS4_Moderate"
            elif sup_lower >= supporting_OR:
                text6 = "The lower bound {}% CI is greater than {} ({}%CI={}). Therefore PS4 is set to".format(
                    supporting_CI, supporting_OR, supporting_CI, round(sup_lower, 2))
                text7 = " PS4_Supporting"
            else:
                text6 = "The lower bound {}% CI is less than {} ({}%CI={}). Therefore PS4 is set to".format(
                    supporting_CI, supporting_OR, supporting_CI, round(sup_lower, 2))
                text7 = " PS4 not met"

            return render(request, 'cardiac/PS4.html', {'form': form,
                                                        'text1': text1,
                                                        'text2': text2,
                                                        'text3': text3,
                                                        'text4': text4,
                                                        'text5': text5,
                                                        'text6': text6,
                                                        'text7': text7,
                                                        })
    else:
        AC = request.GET.get("AC")
        AN = request.GET.get("AN")
        try:
            form = PS4Form(initial={'cases_AC': int(AC), 'cases_AN': int(AN)})
        except:
            form = PS4Form()

    return render(request, 'cardiac/PS4.html', {'form': form})
