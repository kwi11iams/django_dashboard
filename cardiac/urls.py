from django.urls import path
from django.contrib import admin
from django.views.generic.base import RedirectView
from . import views
from .models import Variant

urlpatterns = [
    path('', RedirectView.as_view(url='table/'), name='go-to-table'),
    path("table/", views.VariantListView.as_view(model=Variant), name="table"),
    path("ps4_calculator/", views.ps4_result, name="ps4"),
    path("denominators/", views.denominators, name="denom"),
    path('variant/<int:pk>/', views.variant_detail, name='variant_detail'),
    path('variant/<int:pk>/varaint_details_csv', views.variant_details_csv, name='csv'),
    ]
