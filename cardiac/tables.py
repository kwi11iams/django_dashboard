import django_tables2 as tables
from django.shortcuts import get_object_or_404
from .models import Variant, Occurence, Denominators
from django_tables2.utils import A
from django.db.models import Count

panel_names = {"R132_Twist_DCM-ArCM": "DCM",
            "R131_Twist_HCM": "HCM",
            "R133_Twist_ARVC": "ARVC"}

class VariantTable(tables.Table):
    view_variant = tables.TemplateColumn('<a href="/variant/{{record.variant_ID}}">Details</a>', orderable=False)
    alamut2 = tables.Column(empty_values=(), verbose_name='Alamut',
                            order_by=("chr", "g_coord"))
    # view_on_alamut = tables.TemplateColumn(
    # '<a href="http://localhost:10000/show?request={{record.alamut}}">Alamut</a>')
    count = tables.Column(empty_values=(), verbose_name='Count')
    denom = tables.Column(empty_values=(), verbose_name='Denominators', orderable=False)

    def render_alamut2(self, record):
        variant = get_object_or_404(Variant, pk=record.pk)
        alamut2 = variant.genome_change.replace('g.', '')  # remove 'g.' from string
        return alamut2

    def render_count(self, record):
        variant = get_object_or_404(Variant, pk=record.pk)
        result = Occurence.objects.filter(variant_ID=variant.variant_ID).count()
        return result

    def render_denom(self, record):
        variant = get_object_or_404(Variant, pk=record.pk)
        denoms_qs = Denominators.objects.filter(gene = variant.gene)
        output_txt = ""
        for i in range(denoms_qs.count()):
            i_panel = panel_names[denoms_qs[i].panel]
            i_total = str(denoms_qs[i].total)
            summary_txt = i_panel + ": " + i_total
            if output_txt == "":
                output_txt = summary_txt
            else:
                output_txt = output_txt + " | " + summary_txt

        return output_txt


    def order_count(self, queryset, is_descending):
        queryset = queryset.annotate(
            number_of_occurences=Count('occurence')
        ).order_by(("-" if is_descending else "") + "number_of_occurences")
        return (queryset, True)

    
    class Meta:
        model = Variant
        template_name = "django_tables2/bootstrap.html"
        fields = ("alamut2", "gene", "ref_seq",
                  "variant_type", "c_nomen", "p_nomen", "count", "denom")
        attrs = {'width': '100%'}
