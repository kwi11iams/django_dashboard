from django.db import transaction
import os

from cardiac.models import Variant, Occurence

# primary key for Occurence seems odd when loading this way (missing pk=1 etc)


def run():
    # with transaction.atomic():

    Variant.objects.all().delete()
    Occurence.objects.all().delete()

    variants = []
    occurences = []
    v_count = 0
    o_count = 0
    with open(
        os.path.join(os.path.dirname(__file__), "variant_model.txt"), "rt"
    ) as f:
        for line in f:
            variants.append(line.strip("\n"))
    for var in variants[1:]:
        # variant_ID, ReferencePosition, Gene, Reference, AlternativeAllele, \
        # RefSeq, Chr, VariantType, GenomeChange, MutationCall, \
        # ProteinChange, GenomicCoordinate, Alamut, HGMD_DNA, Build, \
        # pred_artefact = var.split("\t")
        row = var.split("\t")
        # print(row)

        v = Variant(variant_ID=row[0],
                    g_coord=row[1],
                    gene=row[2],
                    ref_base=row[3],
                    alt_base=row[4],
                    ref_seq=row[5],
                    chr=row[6],
                    variant_type=row[7],
                    genome_change=row[8],
                    c_nomen=row[9],
                    p_nomen=row[10],
                    g_nomen=row[11],
                    alamut=row[12],
                    HGMD_DNA=row[13],
                    build=row[14],
                    pred_artefact=row[15])
        v.save()
        v_count += 1

        if v_count % 100 == 0:
            print(v_count)

    print("Go Occurences Go")

    with open(
        os.path.join(os.path.dirname(__file__), "occurence_model.txt"), "rt"
    ) as f:
        for line in f:
            occurences.append(line.strip("\n"))
    for occ in occurences[1:]:
        row = occ.split("\t")
        # print(row)

        occ_2 = Occurence(worklist=row[0],
                          starlims_ID=row[1],
                          mid=row[2],
                          panel=row[3],
                          coverage=row[4],
                          frequency=row[5],
                          score=row[6],
                          variant_type=row[7],
                          homo_hetero=row[8],
                          poss_artefact=row[9]
                          )
        occ_2.variant_ID = Variant.objects.get(variant_ID=int(row[10]))
        occ_2.save()
        o_count += 1

        if o_count % 100 == 0:
            print(o_count)

    return v_count, o_count
