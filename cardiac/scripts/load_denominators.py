import os
from django.db import transaction

from cardiac.models import Denominators

# Append additional denominators to database

def run():
    with transaction.atomic():

        # Denominators.objects.all().delete()

        denominators_upload = []
        d_count = 0

        with open(
            os.path.join(os.path.dirname(__file__), "denominators.txt"), "rt"
        ) as f:
            for line in f:
                denominators_upload.append(line.strip("\n"))
        
        for denom in denominators_upload[1:]:
            if not denom.isspace():
                row = denom.split("\t")
                print(row)

                # See if match for denominator panel and gene, then update information
                qs_match = Denominators.objects.filter(panel = row[0],
                                                    gene = row[1]
                )
                if qs_match.count() == 1:
                    denom_edit = qs_match[0]
                    denom_edit.total = denom_edit.total + int(row[2])
                    denom_edit.save()
                elif qs_match.count() == 0:
                    # Create a new database entry, for unseen gene in panel
                    denom_new = Denominators(panel=row[0],
                                        gene=row[1],
                                        total=row[2]
                                        )
                    denom_new.save()
                else:
                    # raise error, should either exist once or not at all
                    raise Exception("More than one database entry for gene and panel. Please check database. ") 

                d_count += 1

                if d_count % 10 == 0:
                    print(d_count)

    return
