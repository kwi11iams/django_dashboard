from django.test import TestCase, Client
from django.urls import reverse
from cardiac.models import Variant, Occurence, Denominators
import json


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.denominators_url = reverse('denom')
        self.variant_detail_url = reverse('variant_detail', args=['1'])
        self.variant1 = Variant.objects.create(
            variant_ID = 1,
            g_coord = 47364249,
            gene = "MYBPC3",
            ref_base = "G",
            alt_base = "A",
            ref_seq = "NM_000256.3",
            chr = 11,
            variant_type = "UV / Mut? - HGMD",
            genome_change = '11:g.47364249G>A',
            c_nomen = "c.1504C>T",
            p_nomen = "p.(Arg502Trp)",
            g_nomen = 'Chr11.hg19:g.47364249',
            alamut = "11:47364249G>A",
            HGMD_DNA = 'NM_000256.3:c.1504C>T',
            build = 'hg19',
            pred_artefact = 'False',
        )

    def test_denominators_GET(self):
        response = self.client.get(self.denominators_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'cardiac/denominators.html')

    def test_variant_detail_GET(self):
        response = self.client.get(self.variant_detail_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'cardiac/variant_detail.html')