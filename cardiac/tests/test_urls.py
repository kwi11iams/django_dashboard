from django.test import SimpleTestCase, TestCase
from django.urls import reverse, resolve
from django.views.generic.base import RedirectView
from cardiac.views import VariantListView, ps4_result, denominators, variant_detail, variant_details_csv


class TestUrls(SimpleTestCase):

    def test_denominators_url_resolves(self):
        url = reverse('denom')
        self.assertEquals(resolve(url).func, denominators)

    def test_table_url_resolves(self):
        url = reverse('table')
        self.assertEquals(resolve(url).func.view_class, VariantListView)

    def test_ps4_url_resolves(self):
        url = reverse('ps4')
        self.assertEquals(resolve(url).func, ps4_result)

    def test_variant_details_url_resolves(self):
        url = reverse('variant_detail', args=['1'])
        self.assertEquals(resolve(url).func, variant_detail)

    def test_csv_url_resolves(self):
        url = reverse('csv', args=['1'])
        self.assertEquals(resolve(url).func, variant_details_csv)

    def test_blank_url_resolves(self):
        url = reverse('go-to-table')
        print(resolve(url))
        self.assertEquals(resolve(url).func.view_class, RedirectView)
        
        
class TestRedirects(TestCase):

    def test_blank_redirects_to_table(self):
        response = self.client.get('/')
        self.assertRedirects(response, '/table/')


    