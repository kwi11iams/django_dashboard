
from cardiac.views import ps4_calculator, odds_ratio
from django.test import SimpleTestCase

class TestCalculator(SimpleTestCase):

    def setUp(self):
        self.cases = 6
        self.cases_denom = 6000
        self.controls = 2
        self.controls_denom = 60000
        self.CI = 95

    def test_oddsratio(self):
        OR, a, b, c, d = odds_ratio(self.cases, self.controls, self.cases_denom, self.controls_denom)
        self.assertEqual(round(OR,2), 30.03)

    def test_ps4calculator(self):
        lower_bound = ps4_calculator(self.cases, self.controls, self.cases_denom, self.controls_denom, self.CI)
        self.assertEqual(round(lower_bound,2), 6.06)