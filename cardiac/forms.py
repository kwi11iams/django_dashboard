from django import forms
from django.core.exceptions import ValidationError
from .models import Variant_type


class PS4Form(forms.Form):
    # Input fields
    cases_AC = forms.IntegerField(label="Cases - Allele Count", min_value=1)
    cases_AN = forms.IntegerField(label="Cases - Allele Number", min_value=1)
    controls_AC = forms.IntegerField(label="Controls - Allele Count", min_value=1)
    controls_AN = forms.IntegerField(label="Controls - Allele Number", min_value=1)
    strong_OR = forms.IntegerField(label="Strong - OR", initial=20)
    strong_CI = forms.IntegerField(label="Strong - CI", initial=95)
    moderate_OR = forms.IntegerField(label="Moderate - OR", initial=10)
    moderate_CI = forms.IntegerField(label="Moderate - CI", initial=95)
    supporting_OR = forms.IntegerField(label="Supporting - OR", initial=5)
    supporting_CI = forms.IntegerField(label="Supporting - CI", initial=95)

    def clean(self):
        cleaned_data = super().clean()
        cases = cleaned_data.get('cases_AC')
        cases_total = cleaned_data.get('cases_AN')
        controls = cleaned_data.get('controls_AC')
        controls_total = cleaned_data.get('controls_AN')

        # Ensure that count >= number
        if cases and cases_total:
            # Only do something if both fields are valid so far.
            if cases >= cases_total:
                raise ValidationError(
                    "Allele count must be less than the Allele Number"
                )
        if controls and controls_total:
            # Only do something if both fields are valid so far.
            if controls >= controls_total:
                raise ValidationError(
                    "Allele count must be less than the Allele Number"
                )
