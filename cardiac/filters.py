import django_filters
from django.forms import ModelMultipleChoiceField

from .models import Variant, Variant_type
from django.forms.widgets import SelectMultiple, Select, CheckboxSelectMultiple

# TYPE_CHOICES = (
#     ('Poly:Known', 'Poly:Known'),
#     (1, 'UV / Mut?'),
# )

class VariantFilter(django_filters.FilterSet):
    alamut = django_filters.CharFilter(lookup_expr='icontains', label='Alamut Nomenclature')
    ref_seq = django_filters.CharFilter(lookup_expr='icontains', label='Transcript')
    gene = django_filters.CharFilter(lookup_expr='icontains', label='Gene')
    c_nomen = django_filters.CharFilter(lookup_expr='icontains', label='c.nomen')
    g_nomen = django_filters.CharFilter(lookup_expr='icontains', label='Genomic Coordinate')
    # variant_type = django_filters.CharFilter(lookup_expr='icontains', label='Variant Type')
    variant_type = django_filters.ModelMultipleChoiceFilter(field_name='variant_type',
                                                            queryset=Variant_type.objects.all(),
                                                            lookup_expr='icontains',
                                                            widget=SelectMultiple(attrs={'size': 3.5}),
                                                            # widget=CheckboxSelectMultiple,
                                                            label="Variant Type")
                                                            # conjoined=False)

    # gene = django_filters.ModelMultipleChoiceFilter(field_name='gene', queryset=Variant.objects.all())
    # gene = django_filters.ModelMultipleChoiceFilter(queryset=Variant.objects.all(),
    #     widget=forms.CheckboxSelectMultiple)
    #variant_type = django_filters.ChoiceFilter(choices=TYPE_CHOICES)

    class Meta:
        model = Variant
        fields = ["alamut", "g_nomen", "ref_seq", "gene", "variant_type", "c_nomen"]
