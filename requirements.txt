# Libraries compatible with Python3.6
Django==3.2.15
django-tables2==2.4.1
django-extensions==3.1.5
django-filter==21.1
django-crispy-forms==1.13.0
django-debug-toolbar==3.2.4
scipy==1.5.4
tablib==3.1.0